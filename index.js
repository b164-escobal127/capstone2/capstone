const express = require("express");
const mongoose = require("mongoose")
require('dotenv').config();
const cors = require("cors");

const userRoutes = require("./routes/userRoutes")
const productRoutes = require("./routes/productRoutes")

const app = express();



app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended:true }));


app.use("/caps/users", userRoutes);
app.use("/caps/product", productRoutes);

mongoose.connect(process.env.DB_CONNECTION, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once('open', () => console.log('Now connected to MongoDB'));

app.listen(process.env.PORT, () => {
	console.log(`API is now online on port ${process.env.PORT}`)
});
