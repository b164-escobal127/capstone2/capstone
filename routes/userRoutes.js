const express = require("express");
const router = express.Router();
const auth = require("../auth");



const UserController = require("../controllers/userControllers")

router.post("/checkEmail", (req, res) => {
	UserController.checkEmailExists(req.body).then(result => res.send(result))
});

//register user
router.post("/register", (req, res) => {
	UserController.registerUser(req.body).then(result => res.send(result));
});
//authentication
router.post("/login", (req, res) => {
	UserController.loginUser(req.body).then(result => res.send(result));
});

//get all users
router.get("/", (req, res) => {
	UserController.getAllUsers().then(result => res.send(result))
})
//set as admin
router.put("/:userId/setAsAdmin", auth.verify, (req,res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
	UserController.setAsAdmin(req.params.userId, req.body).then(result => res.send(result))
	} else {
		res.send('You are not an admin')
	}
})


//create order
router.post('/order', auth.verify, (req,res) => {
	
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId
	}

	UserController.order(data).then(result => res.send(result));
});

//retrieve order

router.get('/myOrders/:id', auth.verify, (req,res) => {
	
	let data = {
		userId: auth.decode(req.headers.authorization).id
	}

	UserController.myOrders(req.params.id).then(result => res.send(result));
});


router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	UserController.getProfile(userData.id).then(result => res.send(result))
} )











module.exports = router;