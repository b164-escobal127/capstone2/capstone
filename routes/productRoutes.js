const express = require("express");
const router = express.Router();
const auth = require("../auth")
const ProductController = require("../controllers/productControllers");


router.get('/adminProduct', (req, res) =>{
	ProductController.getAllProduct().then(result => res.send(result))
})
//get all active products

router.get("/allActive", (req, res) => {
	ProductController.getAllActive().then(result => res.send(result))

});

//get single product

router.get("/:productId", (req, res) => {
	ProductController.getProduct(req.params.productId).then(result => res.send(result))
})


//creating a product

router.post("/addProduct", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin) {
		ProductController.addProduct(data).then(result => res.send(result));
	} else {
		res.send({auth: "You're not an admin"})
	}
})

//update a product

router.put("/:productId/update", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	console.log

	if(data.isAdmin){
		ProductController.updateProduct(req.params.productId, req.body).then(result => res.send(result));
	} else {
		res.send(false)
	}
})

//archive a product


router.put("/:productId/archive", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	console.log

	if(data.isAdmin){
		ProductController.archivedProduct(req.params.productId, req.body).then(result => res.send(result));
	} else {
		res.send(false)
	}
})




//get all orders 

router.get('/allOrders/:id', auth.verify, (req,res) => {
	
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if (data.isAdmin){
	ProductController.allOrders(req.params.id).then(result => res.send(result));
	} else {
		res.send(false)
	}
});










module.exports = router;