const mongoose = require("mongoose")
const productsSchema = new mongoose.Schema({

	name:{
		type:String,
		required:[true,"name is required"]
	},
	description:{
		type:String,
		required:[true,"description is required"]
	},
	price:{
		type:Number,
		required:[true,"price is required"]
	},
	isAvailable:{
		type:Boolean,
		default:true
	},
	createdOn:{
		type:Date,
		default:new Date()
	},
	orderBy: [{
		userId: {
			type: String,
			required: [true, "UserId is required"]
		},
		purchasedOn: {
			type:Date,
			default: new Date()
		}
	}]
})

module.exports = mongoose.model('Products', productsSchema)
