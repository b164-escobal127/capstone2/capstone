const mongoose = require("mongoose")
const userSchema = new mongoose.Schema({
	
	userName:{
		type:String,
		required:[true,"userName is required"]
	},
	email:{
		type:String,
		required:[true,"email is required"]
	},
	password:{
		type:String,
		required:[true,"password is required"]
	},
	isAdmin:{
		type:Boolean,
		default:false
	},
	order: [{
		productId: {
			type:String,
			required: [true, "Product ID is required"]
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		},
		status: {
			type: String,
			default: "Ordered"
		}
	}]

})

module.exports = mongoose.model('User', userSchema)