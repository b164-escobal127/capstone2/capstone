const Product = require("../models/Products");
const User = require("../models/User");
const auth = require("../auth")


//get all active
module.exports.getAllActive = () => {
	return Product.find({ isAvailable: true }).then(result => {
		return result;
	})
}

module.exports.getAllProduct = () => {
	return Product.find({}).then( result => {
		return result;
	})
}

//get single product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams).then(result => {
		return result;
	})
}

//creating a product

module.exports.addProduct = (reqBody) => {
	let newProduct = new Product({
		name: reqBody.product.name,
		description: reqBody.product.description,
		price: reqBody.product.price
	});
	return newProduct.save().then((product, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	})
}

//update a product

module.exports.updateProduct = (reqParams, reqBody) => {
	let updateProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};
	return Product.findByIdAndUpdate(reqParams, updateProduct).then((product, err) => {
		if(err) {
			return false;
		} else {
			return true;
		}
	})
}


//archived product

module.exports.archivedProduct = (reqParams) => {
	let updatedStatus = {
		isAvailable: false
	};
	return Product.findByIdAndUpdate(reqParams, updatedStatus).then((product, err) => {
		if(err) {
			return false;
		} else {
			return true;
		}
	})
}

//all ordered

module.exports.allOrders = (reqParams) => {
	return User.findById(reqParams).then((result, err) => {
		if(err) {
			return false;
		} else {
			return {orders:result};
		}
	})
}













