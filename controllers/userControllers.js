const User = require("../models/User");

const Products = require("../models/Products")

const bcrypt = require('bcrypt');

const auth = require("../auth")



module.exports.checkEmailExists = (reqBody) => {
	return User.find({ email: reqBody.email }).then(result => {

		if(result.length > 0){
			return true;
		} else {
			return false;
		}
	})
}
//register user
module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		userName: reqBody.userName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	})
};

//authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({ email: reqBody.email}).then(result => {
		if(result == null){
			return false;
		} else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect){
				return { accessToken : auth.createAccessToken(result.toObject())}
			} else{
				return false;
			} 
		}
	})
}

//get all users
module.exports.getAllUsers = () => {
	return User.find({}).then( result => {
		return result;
	})
};


//set as admin
module.exports.setAsAdmin = (reqParams) => {
	let updatedStatus = {
		isAdmin: true
	} 

		return User.findByIdAndUpdate(reqParams, updatedStatus).then((user, error) => {
			if (error) {
				return false
			}else{
				return true
			}
	})

}


//create order 
module.exports.order = async (data) => {
	let orders = await User.findById(data.userId).then(user => {
		user.order.push({ productId: data.productId})
	return user.save().then((user,error) => {
		if (error){
			return false;
		} else{
			return true;
		}
	})
});
	let isOrderUpdated = await Products.findById(data.productId).then(product => { product.orderBy.push({userId: data.userId});
		return product.save().then((product, error) => {
			if(error){
				return false;
			}else {
				return true;
			}
		})

	});

	if (orders && isOrderUpdated){
		return true;
	} else {
		return false;
	}
}

//retrieve order

module.exports.myOrders = (reqParams) => {
	return User.findById(reqParams).then((result, err) => {
		if(err) {
			return false;
		} else {
			return {orders:result};
		}
	})
}


module.exports.getProfile = (data) => {
	return User.findById(data).then(result => {
		
		result.password = "";

		return result;
	})
}






